##### These procedure steps will be followed on the simulator

1. After going through the theory and pretest, click the "Simulation" tab<br>

2. The simulator will display the interactive questions, attempt the questions<br>
<img src="images/s7.png"><br>
3. Click "Submit" button to verify the answers given, click on simulator button to skip the test and go to simulator.<br>
<img src="images/s9.png"><br>
4. Select a medium by clicking on the container symbols<br>
<img src="images/s8.png"><br>
5. Click on "Show" button. This will reveal the apparatus.<br>
<img src="images/s4.jpg"><br>
6. Click on "Switch" button. This will show the light source is on. Note the rings pattern appearing in the field of view of telescope seen at the right top of the screen<br>
<img src="images/s4.2.jpg"><br>
7. Use microscope controls to observe the motion of travelling microscope.<br>
<img src="images/s4.3.jpg"><br>
8. Click on "Next" button to go to the next page of simulator.<br>
<img src="images/s4.4.jpg"><br>
9. The simulator will display the interactive questions, attempt the questions<br>

10. Click on "Show Travelling Microscope" button. This will reveal the experimental setup.<br>
<img src="images/s5.png"><br>
11. Use Microscope movement and lens movement buttons to get resolved ring pattern and set the vertical cross wire at appropriate  position.<br>
<img src="images/s1.png"><br>
12. To note the readings click on "Add to table" button.<br>

13. To show the graph, click on "Generate graph" button.<br>

14. Click on "Conclusion" button<br>

15.  The simulator will display the interactive questions, attempt the questions.<br>

16. Note the conclusions from the experiment performed.<br>
