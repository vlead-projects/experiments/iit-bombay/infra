## Introduction (Round 0)

<br>

<b>Discipline | <b>Engineering
:--|:--|
<b> Lab. | <b> Engineering Physics
<b> Experiment|     <b> 1. Newton's Ring Experiment

<h5> About the Experiment : </h5>
This experiment is about the interference of light. 

<b>Name of Developer | <b> Bhagwan G. Toksha
:--|:--|
<b> Institute | <b> Maharashtra Institute of Technology
<b> Email id|     <b> bhagwan.toksha@mit.asia
<b> Department | Basic Sciences & Humanities

#### Contributors List

SrNo | Name | Faculty or Student | Department| Institute | Email id
:--|:--|:--|:--|:--|:--|
1 | Bhagwan G. Toksha | Faculty | Basic Sciences & Humanities | Maharashtra Institute of Technology, Aurangabad | bhagwan.toksha@mit.asia
2 | Sankalp Pol | Student | Computer Science & Engineering | Maharashtra Institute of Technology, Aurangabad |sankalp.pol@mit.ac.in
3 | Suraj Suryawanshi | Student | Mechanical Engineering | Maharashtra Institute of Technology, Aurangabad |suryawanshisuraj1122@gmail.com
4 | Fardeen Khan  | Student | Civil Engineering | Maharashtra Institute of Technology, Aurangabad |khanfardin510@gmail.com


<br>
