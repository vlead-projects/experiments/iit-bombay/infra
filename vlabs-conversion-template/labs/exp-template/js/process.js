class Process{
  constructor(id, arrivalTime, brustTime) {
    this.id = id;
    this.arrivalTime = arrivalTime;
    this.brustTime = brustTime;
    this.remainingExecutionTime = brustTime;
    this.status = LOADED;
  }

  isComplete(){
    return this.remainingExecutionTime <= 0;
  }

}

function printProcess(process){
  data = []
  for (var i = 0; i < process.length; i++) {
    data.push([
      process[i].id,
      process[i].arrivalTime,
      process[i].brustTime,
      process[i].remainingExecutionTime,
      process[i].status
    ]);
  }
}

function sortprocesses(process){
  return process.sort(function(a,b){
    return a.arrivalTime - b.arrivalTime;
  });
}
