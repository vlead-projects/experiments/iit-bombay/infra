<?php 

require_once 'Parsedown.php';
$parsedown=new Parsedown();

/////////////////////////////////////////
// Write your lab name
////////////////////////////////////////
$lab_name = file_get_contents('round-template/experiment/lab-name.md');

/////////////////////////////////////////
// Write your experiment name
////////////////////////////////////////
$exp_name = file_get_contents('round-template/experiment/experiment-name.md');

$_SESSION['lab_name'] = $lab_name;
$_SESSION['exp_name'] = $exp_name;
?>